<?php

App::uses('AppController', 'Controller');

class UsersController  extends AppController {

	public $name = 'Users';
	public $uses = array('User','Group','Test');

    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'User.username' => 'asc'
        )
    );


    /**
     * Lista todos los usuarios disponibles en el sistema renderiza la informacion en /View/Users/index
     * con la informacion necesaria para ser utilizada por el usuario
     * @uses array $this->paginate - Arreglo asociativo que contiene parametros (limit y Order) para realizar la paginacion de los usuarios
     * @param string $display - parametro para indicar el tipo de usuarios que se quierne visualizar (administrators o professors)
     * @return null
     */
	public function index($display = null)
	{
		$groups = $this->Group->find('all',array('order' => 'Group.name DESC'));

		$this->Paginator->settings = $this->paginate;

		switch($display)
		{
			case "administrators":
				$this->Paginator->settings['conditions'] = array('User.group_id' => 1);
			break;

			case "professors":
				$this->Paginator->settings['conditions'] = array('User.group_id' => 2);
			break;

			default:
				$this->Paginator->settings['conditions'] = array('User.group_id' => 1);
		}

		$users = $this->Paginator->paginate('User');

		$this->set("display",$display);
		$this->set("groups",$groups);
		$this->set("users",$users);
	}


    /**
     * Carga una interfaz para autenticacion de usuarios en /View/Users/login
     * con la informacion necesaria para ser utilizada por el usuario
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function login() 
	{
		if(!empty($this->request->data))
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));


			$user = $this->User->find('first',
						array('conditions' => 
							array(
								'OR' => array(
											'email' => $this->request->data['User']['email'],
											'username' => $this->request->data['User']['email']
										),
								'password' => Security::hash($this->request->data['User']['password'], null, true)
								), 'recursive' => -1
							)
						);
			
			if($user)
			{
				$user['User']['login_method'] = "system";
				$this->Session->write('User', $user['User']);

				switch($this->Session->read('User.group_id'))
				{
					case 1:
						//Administrator
						$this->redirect($this->administratorPaths['default']);
					break;

					case 2:
						//Professor
						$this->redirect($this->professorPaths['default']);
					break;

					default:
						//default behaviour
						$this->redirect(array('controller' => 'Users','action' => 'logout'));
				}
			
			}
			else
			{
				$this->Session->setFlash(__("Usuario o clave de acceso incorrecta. Intenta nuevamente",true));
				$this->redirect(array('controller' => 'Users','action' => 'login'));
			}
		} 
	}

	/**
     * Metodo accesible solo para el administrador que se encarga de dar de alta usuarios en la base de datos
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function create()
	{
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
							  array('odd_spaces' => true,
									'encode' => true,
									'dollar' => true,
									'carriage' => true,
									'unicode' => true,
									'escape' => true,
									'backslash' => true));

			if(!isset($this->request->data['User']['id']))
			{	
				$this->User->create();
			}

			if($this->request->data['User']['password'] == $this->request->data['User']['re-password'])
			{
				/* Object field manipulation */
				if(!empty($this->request->data['User']['password']))
				{
					$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], null, true);
				}
				else
				{
					$user = $this->User->find('first',array('conditions' => array('User.id' => $this->request->data['User']['id'])));
					$this->request->data['User']['password'] = $user['User']['password'];
				}
				/* End Object field manipulation */

				if($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__("Acción completada con exito",true));
					$this->redirect(array('controller' => 'Users','action' => 'index'));
				}
				else
				{
					$this->Session->setFlash(__("Ya existe un usuario con este Email o Mátricula",true));
					$this->redirect(array('controller' => 'Users','action' => 'index'));
				}
			}
			else
			{
				$this->Session->setFlash(__("Los passwords no coinciden",true));
				$this->redirect(array('controller' => 'Users','action' => 'index'));
			}
		}
	}

	/**
     * Metodo accesible solo para el administrador que se encarga de dar de alta masivamente usuarios en la base de datos
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function loadStudents()
	{
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
							  array('odd_spaces' => true,
									'encode' => true,
									'dollar' => true,
									'carriage' => true,
									'unicode' => true,
									'escape' => true,
									'backslash' => true));

			$students = explode("\\n", $this->request->data['User']['students']);
			$cont = 0;

			if(!isset($this->request->data['User']['group_id']))
			{
				$this->request->data['User']['group_id'] = 3;
			}

			for($i = 0; $i < count($students); $i++)
			{
				$student = explode(",", $students[$i]);

				$data['User']['group_id'] = $this->request->data['User']['group_id'];
				$data['User']['username'] = $student[0];
				$data['User']['name'] = $student[1];
				$data['User']['email'] = $student[2];
				$data['User']['password'] = Security::hash($student[3], null, true);
				$data['User']['active'] = 1;

				$this->User->create();
				if($this->User->save($data))
				{
					$cont++;
				}
			}

			$this->Session->setFlash(__("Se agregaron ",true) . $cont . __(" registros nuevos",true));
			$this->redirect(array('controller' => 'Users','action' => 'loadStudents'));			
		}

		$groups = $this->Group->find('all',array('order' => 'Group.name DESC'));
		$this->set('groups', $groups);
	}


	/**
     * Metodo accesible para cualquier usuario en donde muestra la informacion basica sobre su cuenta, el metodo utiliza
     * el archivo que se encuentra en app/Views/Users/settings.ctp como interfaz de usuario
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function settings()
	{
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
							  array('odd_spaces' => true,
									'encode' => true,
									'dollar' => true,
									'carriage' => true,
									'unicode' => true,
									'escape' => true,
									'backslash' => true));

			if($this->request->data['User']['password'] == $this->request->data['User']['re-password'])
			{
				/* Object field manipulation */
				if(!empty($this->request->data['User']['password']))
				{
					$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], null, true);
				}
				else
				{
					$user = $this->User->find('first',array('conditions' => array('User.id' => $this->userData['id'])));
					$this->request->data['User']['password'] = $user['User']['password'];
				}

				$this->request->data['User']['id'] = $this->userData['id'];
				unset($this->request->data['User']['name']);
				unset($this->request->data['User']['username']);
				unset($this->request->data['User']['email']);
				unset($this->request->data['User']['group_id']);
				/* End Object field manipulation */

				if($this->User->save($this->request->data))
				{
					//Updation Session data
					$this->Session->write('User.password', $this->request->data['User']['password']);

					$this->Session->setFlash(__("Acción completada con exito",true));
					$this->redirect(array('controller' => 'Users','action' => 'settings'));
				}
				else
				{
					$this->Session->setFlash(__("Ocurrio un error!",true));
					$this->redirect(array('controller' => 'Users','action' => 'settings'));
				}
			}
			else
			{
				$this->Session->setFlash(__("Los passwords no coinciden",true));
				$this->redirect(array('controller' => 'Users','action' => 'settings'));
			}
		}
		else
		{
			$groups = $this->Group->find('all',array('order' => 'Group.name DESC'));
			$user['User'] = $this->userData;
			$this->set('groups',$groups);
			$this->set('user',$user);
		}		
	}


	/**
     * Metodo accesible para cualquier usuario que permite cerrar (destruir) la sesion actual en el sistema de manera segura
     * @uses array $this->Session - Arreglo que se almacena como una variable de session y que contiene la informacion del usuario
     * que esta actualmente autenticado en la plataforma
     * @param null
     * @return null
     */
	public function logout()
	{
		if($this->Session->check('User'))
		{
			$this->Session->destroy();	
		}		
		$this->redirect(array('controller' => 'Users','action' => 'login'));
	}

	/**
     * Metodo accesible solo para el administrador que se encarga de eliminar registro de usuarios de la base de datos
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function delete()
	{
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			foreach($this->request->data['User']['id'] as $key => $value)
			{
				//key: test id
				//value: 1 = delete / 0 = dont delete

				if($value)
				{
					$this->User->delete($key);
					$this->Test->deleteAll(array('Test.user_id' => $key), true);
				}
			}

			$this->Session->setFlash(__("Acción completada con exito",true));
			$this->redirect(array('controller' => 'Users','action' => 'index'));
		}
		else
		{
			$this->redirect(array('controller' => 'Users','action' => 'index'));
		}
	}

	/**
     * Metodo accesible solo para el administrador mediante ajax que se encarga de activar o desactivar un usuario, esto con la
     * finalidad de que pueda utilizar la plataforma
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @param null
     * @return null
     */
	public function activate()
	{
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			$user = $this->User->find('first',array('conditions' => array('User.id' => $this->request->data('user'))));

			if($user['User']['active'])
			{
				$user['User']['active'] = 0;
			}
			else
			{
				$user['User']['active'] = 1;
			}

			if($this->User->save($user))
			{
				echo "Ok";
			}
			else
			{
				echo "Fail";
			}
			
		}
	}

	/**
     * Metodo accesible solo para el administrador mediante ajax que se encarga editar la informacion de los usuarios en la base de datos
     * @uses int $this->request->data('user') - Numero identificador unico para referenciar a un usuario en la base de datos
     * @param null
     * @return null
     */
	public function editUser()
	{
		$this->autoRender = false;
		if ($this->request->is('ajax')) 
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			$user = $this->User->find('first',array('conditions' => array('User.id' => $this->request->data('user'))));
			$groups = $this->Group->find('all',array('order' => 'Group.name DESC'));

			$this->set('user',$user);
			$this->set("groups",$groups);
			$this->render('/Elements/edit_user'); 
		}		
	}

}

?>