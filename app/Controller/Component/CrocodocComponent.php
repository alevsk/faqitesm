<?php  
 
App::uses('Component', 'Controller');
App::import('vendor', 'Crocodoc', array('file' =>'crocodoc/Crocodoc.php'));

class CrocodocComponent extends Object {

  protected $_Collection;
  protected $_componentMap = array();
  public $settings = array();
  public $components = array('Session');

  public $Controller = null;
  public $CrocoDoc = null;

  public function __construct(ComponentCollection $collection, $settings = array()) {
    $this->_Collection = $collection;
    $this->settings = $settings;
    $this->_set($settings);
    
    if (!empty($this->components)) {
      $this->_componentMap = ComponentCollection::normalizeObjectArray($this->components);
    }



  }

  public function __get($name) {
    if (isset($this->_componentMap[$name]) && !isset($this->{$name})) {
      $settings = array_merge((array)$this->_componentMap[$name]['settings'], array('enabled' => false));
      $this->{$name} = $this->_Collection->load($this->_componentMap[$name]['class'], $settings);
    }
    if (isset($this->{$name})) {
      return $this->{$name};
    }
  }

  public function initialize(Controller $controller)
  {
    $this->Controller = $controller;
  }

  public function startup(Controller $controller)
  {
    
  }

  public function beforeRender(Controller $controller)
  {

  }

  public function shutdown(Controller $controller)
  {

  }

  public function beforeRedirect(Controller $controller, $url, $status = null, $exit = true)
  {
  
  }

  public function CrocodocDocument()
  {

  }

  public function CrocodocDownload()
  {

  }

  public function CrocodocSession()
  {

  }

  public function getApiLibrary()
  {
    
    $this->CrocoDoc = new Crocodoc();
    $this->CrocoDoc->setApiToken("sJoaS8tgrWelxnVihb1kZ4KT");

    return $this->CrocoDoc;
  }
}

?>