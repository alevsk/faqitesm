<?php

App::uses('AppController', 'Controller');

/**
 * Controlador del Modelo Resource, contiene los metodos necesarios para la comunicacion entre las vistas y la base de datos
 * @var string $name - Nombre para hacer referencia a este controlador (convencion hecha en CakePHP)
 * @var array $uses - Contiene una lista de Modelos a utilizar en este controlador
 * @var array $paginate - Contiene una lista de propiedades para que los documentos sean filtrados a la hora de ser llamados de la base de datos
 * @var array $fileTypes - Contiene un arreglo de formatos permitidos de archivos para ser cargados en la aplicacion
 * @var array $fileExtension - Contiene un arreglo de extensiones permitidas de archivos para ser cargados en la aplicacion
 */
class ResourcesController  extends AppController {

	/**
	 * Nombre para hacer referencia a este controlador (convencion hecha en CakePHP)
	 *
	 * @var string
	 */
	public $name = 'Resources';

	/**
	 * Contiene una lista de Modelos a utilizar en este controlador
	 *
	 * @var array
	 */
	public $uses = array('Resource','ResourceTag','Tag');

	/**
	 * Contiene una lista de propiedades para que los documentos sean filtrados a la hora de ser llamados de la base de datos
	 *
	 * @var array
	 */
	public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Resource.name' => 'asc'
        )
    );

	/**
	 * Contiene un arreglo de formatos permitidos de archivos para ser cargados en la aplicacion
	 *
	 * @var array
	 */
	public $fileTypes = array("application/pdf");

	/**
	 * Contiene un arreglo de extensiones permitidas de archivos para ser cargados en la aplicacion
	 *
	 * @var array
	 */	
	public $fileExtension = array("pdf","PDF");

	/**
     * Lista todos los documentos (Resources) disponibles en el sistema renderiza la informacion en /View/Resources/index
     * con la informacion necesaria para ser utilizada por el usuario
     * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
     * @uses string $this->request->data['Resource']['tags'] - Contiene una lista de etiquetas en la forma (etiqueta1,etiqueta2,etiqueta3,etc ...)
     * @param null
     * @return null
     */
	public function search() {

		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
							  array('odd_spaces' => true,
									'encode' => true,
									'dollar' => true,
									'carriage' => true,
									'unicode' => true,
									'escape' => true,
									'backslash' => true));

			$tags = split(',', $this->request->data['Resource']['tags']);
			$tagsInfo = array();
			$tagIds = array();

			for($i = 0; $i < count($tags); $i++)
			{
				$tagsInfo[$i] = $this->getTagInfoForSearch($tags[$i]);
				$tagIds[$i] = $tagsInfo[$i]['Tag']['id'];
			}
			
			$resources = $this->Tag->find('all', array('recursive' => 2, 'conditions' => array('Tag.id' => $tagIds), 'group' => array('Tag.id')));
			
			$this->set('tagsInfo',$tagsInfo);
			$this->set('resources',$resources);
		}
	}

    /**
     * Lista todos los documentos (Resources) disponibles en el sistema renderiza la informacion en /View/Resources/index
     * con la informacion necesaria para ser utilizada por el usuario
     * @uses array $this->paginate - Arreglo asociativo que contiene parametros (limit y Order) para realizar la paginacion de los documentos
     * @param null
     * @return null
     */
	public function index() {

		$this->Paginator->settings = $this->paginate;
		$resources = $this->Paginator->paginate('Resource');
	    $this->set("resources",$resources);

	}

	/**
	 * Metodo para dar de alta o editar documentos (Resources) en el sistema
	 * recibe el array $this->request->data con toda la informacion necesaria
	 * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
	 * @param null
	 * @return null
	 */
	public function create() {
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
							  array('odd_spaces' => true,
									'encode' => true,
									'dollar' => true,
									'carriage' => true,
									'unicode' => true,
									'escape' => true,
									'backslash' => true));

			if(!isset($this->request->data['Resource']['id']))
			{

				////////////
				//Check file type
				////////////
				if(in_array($this->request->data['Resource']['url']['type'], $this->fileTypes))
				{
					 if ($this->request->data['Resource']['url']['error'] == 0) 
					 {

					 	//file is valid
					 	$fileName = $this->request->data['Resource']['url']['name'];

					 	//Check file extension
					 	if(in_array(substr(strrchr($fileName,'.'),1), $this->fileExtension))
					 	{
					 		$extension = substr(strrchr($fileName,'.'),1);
						 	$fileName = Security::hash(date("Y-m-d,h:m:s")) . '.' .$extension;

						 	$uploadPath = WWW_ROOT . 'files';
						 	$full_file_path = $uploadPath . DS  . $fileName;

						 	if(move_uploaded_file($_FILES['data']['tmp_name']['Resource']['url'], $full_file_path)) 
						 	{
						 		$filePath = Router::url('/files/') . $fileName;
						 	}
						 	else
						 	{
						 		$this->Session->setFlash(__("Ocurrio un error al guardar el documento",true));
						 		$this->redirect("/");
						 	}

					 	}
					 	else
					 	{
							$this->Session->setFlash(__("Solo se aceptan documentos en formato PDF",true));
							$this->redirect("/");				 		
					 	}

					 }
					 else
					 {
					 	$this->Session->setFlash(__("Ocurrio un error al guardar el documento",true));
					 	$this->redirect("/");
					 }
				}
				else
				{
					$this->Session->setFlash(__("Seleccione un documento pdf valido",true));
					$this->redirect(array('controller' => 'Resources', 'action' => 'index'));
				}

				$this->request->data['Resource']['url'] = $filePath;

				////////////
				
				$this->request->data['Resource']['active'] = 1;
				$this->Resource->create();
			}

			if($this->Resource->save($this->request->data))
			{
				if(isset($this->request->data['Resource']['id']))
				{
					$resourceId = $this->request->data['Resource']['id'];

					/** Eliminamos las relaciones actuales para remplazarlas por las nuevas **/
					$this->ResourceTag->deleteAll(array('ResourceTag.resource_id' => $resourceId)); 
				}
				else
				{
					$resourceId = $this->Resource->getLastInsertID();
				}

				$tags = split(',', $this->request->data['Resource']['tags']);
				for($i = 0; $i < count($tags); $i++)
				{
					$this->ResourceTag->create();
					$resourceTag['ResourceTag']['tag_id'] = $this->getTagInfo($tags[$i]);
					$resourceTag['ResourceTag']['resource_id'] = $resourceId;

					$this->ResourceTag->create();
					$this->ResourceTag->save($resourceTag);
				}

				$this->Session->setFlash(__("Acción completada con exito",true));
				$this->redirect(array('controller' => 'Resources','action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__("Ocurrio un error!",true));
				$this->redirect(array('controller' => 'Resources','action' => 'index'));
			}

		}
		else
		{
			$this->redirect(array('controller' => 'Resources','action' => 'index'));
		}

	}

	/**
	 * Metodo para eliminar documentos (Resources) en el sistema
	 * recibe el array $this->request->data que contiene los ids de los
	 * documentos a eliminar de la base de datos
	 * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
	 * @param null
	 * @return null
	 */
	public function delete() {
		if($this->request->data)
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			foreach($this->request->data['Resource']['id'] as $key => $value)
			{
				//key: Resource id
				//value: 1 = delete / 0 = dont delete

				if($value)
				{
					$this->Resource->delete($key);
				}
			}

			$this->Session->setFlash(__("Acción completada con exito",true));
			$this->redirect(array('controller' => 'Resources','action' => 'index'));
		}
		else
		{
			$this->redirect(array('controller' => 'Resources','action' => 'index'));
		}
	}

	/**
	 * Metodo para activar o desactivar la visibilidad de los documentos en el sistema (Resources),
	 * recibe el array $this->request->data por medio de ajax que contiene el id del documento
	 * $this->request->data('resource') e intercambia el valor del campo ['Resource']['active'] entre 0 y 1 en la base datos
	 * de un registro en particular
	 * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
	 * @uses int $this->request->data('resource') - El id del documento (Resource)
	 * @param null
	 * @return null
	 */
	public function activate(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			$resource = $this->Resource->find('first',array('conditions' => array('Resource.id' => $this->request->data('resource'))));

			if($resource['Resource']['active'])
			{
				$resource['Resource']['active'] = 0;
			}
			else
			{
				$resource['Resource']['active'] = 1;
			}

			if($this->Resource->save($resource))
			{
				echo "Ok";
			}
			else
			{
				echo "Fail";
			}
			
		}
	}

	/**
	 * Metodo para requerir un documento (Resource) en particular
	 * recibe el array $this->request->data por medio de ajax que contiene el id del documento 
	 * $this->request->data('resource'), cakePHP renderiza el elemento "/Elements/view_resource" con la informacion
	 * necesaria que despues es regresado a la vista desde donde se invoco el metodo
	 * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
	 * @uses int $this->request->data('resource') - El id del documento (Resource)
	 * @param null
	 * @return null
	 */
	public function getResource()
	{
		$this->autoRender = false;
		if ($this->request->is('ajax')) 
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));

			$resource = $this->Resource->find('first',array('conditions' => array('Resource.id' => $this->request->data('resource'))));
			$this->set('resource',$resource);
			$this->render('/Elements/view_resource'); 
		}
	}

	/**
	 * Metodo para editar un documento (Resource) en particular
	 * recibe el array $this->request->data por medio de ajax que contiene el id del documento 
	 * $this->request->data('resource'), cakePHP renderiza el elemento "/Elements/edit_Resource" con la informacion
	 * necesaria que despues es regresado a la vista desde donde se invoco el metodo
	 * @uses array $this->request->data - Arreglo asociativo que contiene parametros enviados desde la vista al controlador
	 * @uses int $this->request->data('resource') - El id del documento (Resource)
	 * @param null
	 * @return null
	 */
	public function editResource()
	{
		$this->autoRender = false;
		if ($this->request->is('ajax')) 
		{
			$this->request->data = Sanitize::clean($this->request->data,
										  array('odd_spaces' => true,
												'encode' => true,
												'dollar' => true,
												'carriage' => true,
												'unicode' => true,
												'escape' => true,
												'backslash' => true));


			$resource = $this->Resource->find('first',array('conditions' => array('Resource.id' => $this->request->data('resource'))));
			$tagIds = array();

			foreach($resource['ResourceTag'] as $tag)
			{
				$tagIds[] = $tag['tag_id'];
			}

			$tags = $this->Tag->find('all', array('fields' => array('Tag.name'), 'recursive' => -1, 'conditions' => array('Tag.id' => $tagIds)));
			
			$tagString = "";
			foreach($tags as $tag)
			{
				$tagString .= $tag['Tag']['name'] . ",";
			}

			$this->set('resource',$resource);
			$this->set('tagString',$tagString);
			$this->render('/Elements/edit_Resource'); 
		}		
	}

	/**
	 * Metodo para visualizar los documentos (Resource) asignados a un examen en particular
	 * recibe el array $this->request->data que contiene el id del examen 
	 * $this->request->data('resource'), cakePHP renderiza el elemento "/View/Resources/view" con la informacion
	 * necesaria 
	 * @param int $id - id del documento (Resource) ha visualizar
	 * @return null
	 */
	public function view($id = null) 
	{

		$id = (int) $id;

		$test = $this->UserTest->find('first', array('conditions' => array('UserTest.user_id' => $this->userData['id'], 'UserTest.test_id' => $id)));

		if($test)
		{
			$this->Paginator->settings = array('limit' => 10);
			$this->Paginator->settings['conditions'] = array('ResourceTest.test_id' => $test['Test']['id']);
			$resourceTest = $this->Paginator->paginate('ResourceTest');

			$this->set('test', $test);
			$this->set('resourceTest', $resourceTest);
		}
		else
		{
			$this->Session->setFlash(__("Ocurrio un error!",true));
			$this->redirect(array('controller' => 'Tests','action' => 'index'));			
		}
	}

	/**
	 * Metodo disponible solo mediante AJAX que devuelve un listado de etiquetas existentes en la base de datos
	 * en base a un termino dado
	 * @uses $_GET['term'] - termino proporcionado para devolver los resultados que coincidan de la base de datos
	 * @param null
	 * @return json_array $autoCompleteArray - objeto json que contiene etiquetas 
	 */
	public function getTags()
	{
		$this->autoRender = false;
		if ($this->request->is('ajax')) 
		{
			$term = Sanitize::clean($_GET['term'],
													  array('odd_spaces' => true,
															'encode' => true,
															'dollar' => true,
															'carriage' => true,
															'unicode' => true,
															'escape' => true,
															'backslash' => true));



			$tags = $this->Tag->find('all', array('limit' => 10, 'conditions' => array('Tag.name LIKE' => "%$term%")));
			$autoCompleteArray = array();
			$i = 0;

			foreach($tags as $tag)
			{
				$autoCompleteArray[$i]['id'] = $tag['Tag']['name'];
				$autoCompleteArray[$i]['label'] = $tag['Tag']['name'];
				$autoCompleteArray[$i]['value'] = $tag['Tag']['name'];
				$i++;
			}
			
			echo json_encode($autoCompleteArray);			
		}
	}

	/**
	 * Metodo que devuelve informacion acerca de una etiqueta en base a su nombre, de no existir la etiqueta en la base
	 * de datos, sera creada y se devolvera el id del nuevo registro
	 * @param string $name - nombre de la etiqueta
	 * @return int $id - id de la etiqueta en la base de datos, si la etiqueta ya existe se devolvera el id
	 * en el caso contrario se insertara en la base de datos y se devolvera el id del nuevo registro
	 */
	private function getTagInfo($name)
	{
		$tag = $this->Tag->find('first', array('conditions' => array('Tag.name' => $name)));
		if($tag)
		{
			/** La etiqueta ya existe en la base de datos y solo devolvemos el id **/
			return $tag['Tag']['id'];
		}
		else
		{
			/** La etiqueta no existe en la base de datos, procedemos a insertar el nuevo registro y devolvemos el id **/
			$newTag['Tag']['name'] = $name;
			$newTag['Tag']['slug'] = $this->createSlug($name);

			$this->Tag->create();
			$this->Tag->save($newTag);

			return $this->Tag->getLastInsertID();
		}
	}

	/**
	 * Metodo que devuelve informacion acerca de una etiqueta en base a su nombre, de no existir la etiqueta en la base
	 * de datos, sera creada y se devolvera el id del nuevo registro
	 * @param string $name - nombre de la etiqueta
	 * @return Tag $tag - Objeto Tag que contiene toda la informacion de la etiqueta
	 */
	private function getTagInfoForSearch($name)
	{
		$tag = $this->Tag->find('first', array('conditions' => array('Tag.name' => $name)));
		if($tag)
		{
			/** La etiqueta existe en la base de datos **/
			return $tag;
		}
	}

	/**
	 * Metodo que genera un slug (texto simplificado) en base a una cadena de texto dada
	 * @param string $text - cadena de texto
	 * @return string $text - version simplificada de la cadena de texto
	 */
	private function createSlug($text)
	{ 
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	  $text = trim($text, '-');
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	  $text = strtolower($text);
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if(empty($text))
	  {
	    return 'n-a';
	  }

	  return $text;
	}
}

?>