<?php

class Resource extends Model {

  var $name = 'Resource';

	var $hasMany = array(
        'ResourceTag' => array(
            'className' => 'ResourceTag',
            'foreignKey' => 'resource_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'dependent'=> true,
        ),
	);
}

?>