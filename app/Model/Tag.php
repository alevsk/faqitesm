<?php

class Tag extends Model {

  var $name = 'Tag';

	var $hasMany = array(
        'ResourceTag' => array(
            'className' => 'ResourceTag',
            'foreignKey' => 'tag_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'dependent'=> true,
        ),
	);

}

?>