<?php

class ResourceTag extends Model {

  var $name = 'ResourceTag';
  var $useTable = 'resource_tags';

  var $belongsTo = array(
    'Resource' => array(
        'className' => 'Resource',
        'foreignKey' => 'resource_id',
        'conditions' => '',
        'fields' => '',
        'order' => ''
    ),
   'Tag' => array(
        'className' => 'Tag',
        'foreignKey' => 'tag_id',
        'conditions' => '',
        'fields' => '',
        'order' => '',
        'counterCache' => 'resource_tag_count',
    ),
);

}

?>