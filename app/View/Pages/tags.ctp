<script language="javascript" type="text/javascript">
$(function() {

  $('#ResourceTags').tagsInput({
    	width: 'auto',
    	height:'auto',
    	defaultText: 'agregar etiquetas',
        autocomplete_url:"<?php echo $this->Html->url(array('controller' => 'Resources', 'action' => 'getTags'))?>",
    });
});
</script>
<style type="text/css">
  	#ResourceTags_tag
  	{
  		width: 130px;
  	}
  	#ResourceTags_tagsinput{
    	margin-bottom:10px !important;
    	padding-bottom:0px !important;
  	}
	.ui-menu .ui-menu-item a {
		font-size: 14px !important;
	}

	.home_tags
	{
		border: 1px solid #a5d24a;
		-moz-border-radius: 2px;
		-webkit-border-radius: 2px;
		display: block;
		float: left;
		padding: 5px;
		text-decoration: none;
		background: #cde69c;
		color: #638421;
		margin-right: 5px;
		margin-bottom: 5px;
		font-family: helvetica;
		font-size: 13px;
	}
	.home_tags a
	{
		color: #638421;
	}
</style>

<div class="row collapse">

	<?php echo $this->Form->create('Resource',array('class' => 'custom','url' => array('controller' => 'Resources','action' => 'search'))); ?>
	<div class="large-6 large-centered columns">
		<div class="row" style="text-align:center;">
			<h2><?php echo __('Busqueda de documentos'); ?></h2>
		</div>
		<div class="row collapse">
			<div class="small-12 columns">
				<?php echo $this->Form->input('tags', array('label'=> false, 'class' => 'tags', 'type' => 'text')); ?>
			</div>
			<div class="small-2 columns">
				<?php echo $this->Form->submit(__('Buscar',true),array('class' => 'button postfix')); ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>

	<hr>

	<?php if(isset($resources)) { ?>

		<div class="row collapse" id="updates">

			<div class="row collapse">
				<h4 class="subheader"><?php echo __('Documentos encontrados'); ?></h4>
			</div>

			<div class="row collapse">

				<?php foreach($resources as $tag) { ?>

					<div class="row collapse">

						<span class="home_tags"><a href="#"><?php echo $tag['Tag']['name']; ?> x <strong><?php echo $tag['Tag']['resource_tag_count']; ?></strong></a></span>

						<table id="tags-table">
						  <thead>
						    <tr>
						      <th style="text-align:left;"><?php echo __('Nombre del documento'); ?></th>
						      <th width="200"><?php echo __('Visualizar'); ?></th>
						    </tr>
						  </thead>
						  <tbody>
							<?php foreach($tag['ResourceTag'] as $resource) { ?>

							    <tr>
							      <td style="text-align:left;"><?php echo $resource['Resource']['name']; ?></td>
							      <td>
							      	<a href="javascript:void(0)" class="button small secondary viewResource" data-reveal-ajax="true" data-resource-id="<?php echo $resource['Resource']['id']; ?>"><i class="fa fa-eye" style="font-size: 15px;font-weight: normal;"></i></a>
							      </td>
							    </tr>

							<?php } ?>
						  </tbody>
						</table>
					</div>
				<?php } ?>
			</div>
		</div>

	<?php } else { ?>

		<div class="row collapse" id="updates">

			<div class="row collapse">
				<h4 class="subheader"><?php echo __('Se muestran todas las categorias') . " (" . count($tags) . ") "; ?></h4>
			</div>

			<div class="row collapse">


					<div class="row collapse">

						<table id="tags-table">
						  <thead>
						    <tr>
						      <th style="text-align:left;"><?php echo __('Nombre del documento'); ?></th>
						      <th width="200"><?php echo __('Visualizar'); ?></th>
						    </tr>
						  </thead>
						  <tbody>
							<?php foreach($tags as $tag) { ?>

							    <tr>
							      <td style="text-align:left;"><span class="home_tags"><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'tags', $tag['Tag']['slug'])); ?>"><?php echo $tag['Tag']['name']; ?></td>
							      <td>
							      	<a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'tags', $tag['Tag']['slug'])); ?>">
							      		<li class="general foundicon-search"></li>
							      		<?php echo __('Revisar ') . $tag['Tag']['resource_tag_count'] . __(' documentos'); ?>
							      	</a>
							      </td>
							    </tr>

							<?php } ?>
						  </tbody>
						</table>
					</div>
			</div>
		</div>
		
	<?php } ?>
</div>

<div id="tempResourceModal" class="reveal-modal white-modal" style="">
  <a class="close-reveal-modal">&#215;</a>
  <div class="modalContent"></div>
</div>

<!-- Jquery Form Validation Code -->
<script>
	$(document).ready(function(){

		$('.home_tags').mouseenter(function() {
  			$( this ).animate({
			    fontSize : "25px"
			  }, 100, function() {
			  });
		});

		$('.home_tags').mouseleave(function() {
  			$( this ).animate({
			    fontSize : "13px"
			  }, 100, function() {
			  });
		});

		$("#tags-table").dataTable({
			"oLanguage": {
			       "sZeroRecords": "<?php echo __('No se encontraron registros'); ?>",
			       "sSearch": "",
			       "sInfo": "",
			    },
	 		 "lengthChange": false,
	 	}); 

		$('.dataTables_filter input').attr("placeholder", "<?php echo __('Filtrar categorias por titulo'); ?>");

		$('.viewResource').click(function(){
			$.ajax({
				type: "POST",
	    		url: "<?php echo $this->Html->url(array('controller' => 'Resources','action' => 'getResource')); ?>",
	    		data: "resource="+$(this).data('resource-id'),
	    		success: function(data) {
	              $('#tempResourceModal .modalContent').html(data);
	              $('#tempResourceModal').foundation('reveal', 'open');
	    		}
	  		});
		});

	    $("#tempResourceModal").bind('closed', function() {
	      $('#tempQuestionModal .modalContent').html("");
	    });

	});
</script>
<!-- Jquery Form Validation Code -->