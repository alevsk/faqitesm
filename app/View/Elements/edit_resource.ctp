<style type="text/css">
	#ResourceUrlEdit{
		width:100%;
		height: 32px;
	}
	.ui-autocomplete-input
	{
		width: 100% !important;
	}
  	.tagsinput{
    	margin-bottom:10px !important;
    	padding-bottom:0px !important;
  	}
  	.ui-widget-content
  	{
  		z-index: 100 !important;
  	}
</style>
<div class="row collapse">
	<h2><?php echo __('Editar documento'); ?></h2>
	<div class="panel row collapse no-color-panel">
		<?php echo $this->Form->Create('Resource', array('type' => 'file', 'class' => 'custom', 'id' => 'ResourceCreateFormEdit', 'url' => array('action' => 'create'))); ?>
		<?php echo $this->Form->input('Resource.id',array('type' => 'hidden','label' => false, 'value' => $resource['Resource']['id'],'placeholder' => __('Escriba un nombre para este documento',true))); ?>
		<div class="row collapse">
			<div class="large-4 columns left">
				<span class="label"><?php echo __('Nombre del recurso'); ?></span>
				<?php echo $this->Form->input('Resource.name',array('required' => '', 'type' => 'text','label' => false, 'value' => $resource['Resource']['name'],'placeholder' => __('Escriba un nombre para este documento',true))); ?>
			</div>
			<div class="large-8 columns left">
				<span class="label" id="doc-span-edit"><?php echo __('No hay un documento seleccionado'); ?></span>
				<?php  echo $this->Form->file('Resource.url', array('label'=> false, 'class' => 'custom-file-input', 'id' => 'ResourceUrlEdit', 'disabled' => true)); ?>
				<?php //echo $this->Form->input('Resource.url',array('required' => '', 'type' => 'text','label' => false, 'placeholder' => __('Escriba una url valida',true))); ?>
			</div>
		</div>
		<div class="row collapse">
		<span class="label" style="width:100%;"><?php echo __('Etiquetas para este documento'); ?></span>
			<?php echo $this->Form->input('tags', array('id' => 'ResourceTagsEdit', 'label'=> false, 'class' => 'tags ResourceTagsEdit', 'type' => 'text', 'value' => $tagString)); ?>
		</div>
		<div class="row collapse">
			<?php echo $this->Form->submit(__('Editar',true),array('class' => 'button small')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){

		var currentFullPath = "<?php echo $resource['Resource']['url']; ?>";
		var currentFile = currentFullPath.replace(/^.*[\\\/]/, '');
		$('#doc-span-edit').html('<strong>' + currentFile + '</strong>');


		$('#ResourceUrlEdit').change(function(){ 

			if($('#ResourceUrlEdit').val() === "")
			{
				$('#doc-span-edit').html('<?php echo __("No hay un documento seleccionado"); ?>');
			}
			else
			{
				var fullPath = $('#ResourceUrlEdit').val();
				var filename = fullPath.replace(/^.*[\\\/]/, '');
				$('#doc-span-edit').html('<strong>' + filename + '</strong>');
			}

		});

     	$('#ResourceCreateFormEdit').validate({
        	errorClass: 'error',
        	rules: {
          		"data[Resource][name]": {
            		required: true,
          		},
        	}
      	});

      	$('.ResourceTagsEdit').tagsInput({
	    	width: 'auto',
	    	height:'auto',
	    	defaultText: 'agregar',
	        autocomplete_url:"<?php echo $this->Html->url(array('controller' => 'Resources', 'action' => 'getTags'))?>",
    	});
      	
    });
</script>
<!-- Jquery Form Validation Code -->